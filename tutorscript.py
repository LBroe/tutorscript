#!/usr/bin/python3

import sys
import os
import shutil
import subprocess
import re
import csv
import argparse
import datetime

import math

LINUX = True

if LINUX:
    # Filebrowser is opened when starting the script and closed in the end. Can be set to None.
    FILEBROWSER = "nautilus"

    PDF_EDITOR = "evince"
else:
    FILEBROWSER = None
    PDF_EDITOR = "C:\Program Files (x86)\Adobe\Acrobat Reader DC\Reader\AcroRd32.exe"

DR_RACKET = "C:\Program Files\Racket\DrRacket.exe"

ZIP_PREFIX = "RS-2019_1-"

UNGRADED_FOLDER = "Abgabe_Unbewertet"
GRADING_FOLDER = "Abgabe_Bewertung"
RESORTED_FOLDER = "Abgabe_Einsortiert"
NOTES_FILE = "Notizen.csv"

GROUP_COLUMN_NAME = "Gruppe"
GRADE_COLUMN_NAME = "Bewertung"

DASHES_IN_GROUPNAME = 0 # How many dashes are in the names of the groups.
GROUPNAME_REGEX = re.compile(r"(?P<groupname>.*?" + "-.*?" * DASHES_IN_GROUPNAME + ")-.*")
EXCLUDE_GROUPNAMES_REGEX = re.compile(r"(Nicht Mitglied einer Gruppe.).*")

parser = argparse.ArgumentParser(
    description="A little ugly script for unpacking, grading and repacking Learnweb grading stuff.",
    epilog=(f"The zip will be unpacked, the contents will be put to <exercise name>/{UNGRADED_FOLDER}.\n\n\n"
            f"The files will be picked out of the folders for each person and will be moved to <exercise name>/{GRADING_FOLDER} with a "
            f"prefix for their group.\n"
            f"If a group submitted a zip file, that will be unpacked too. I try to do useful things with the content.\n"
            f"While grading, the grades, notes and 'sinnvoll bewerted' values will be written in <exercise name>/{NOTES_FILE}.\n"
            f"When packing, the files from {GRADING_FOLDER} will be resorted into <exercise name>/{RESORTED_FOLDER} and then zipped,"
            f"the resulting zip file is saved in <exercise name>.\n"
            f"The grades from {NOTES_FILE} will be moved to a csv file in <exercise name>. The notes won't be copied, they are for you personally.\n\n"
            f"IMPORTANT: Do all your grading in {GRADING_FOLDER}, every other folder might be overridden or deleted."),
    formatter_class=argparse.RawTextHelpFormatter)

parser.add_argument("work_dir",
                    help="The working directory which contains the .zip and .csv files")

parser.add_argument("-u", "--unpack",
                    help="Only unpack the .zip files (except when another option is given)",
                    action="store_true")

parser.add_argument("-g", "--grade",
                    help="Only grade the extracted files (except when another option is given)",
                    action="store_true")

parser.add_argument("-p", "--pack",
                    help="Only pack the .zip files and transfer grades (except when another option is given)",
                    action="store_true")

parser.add_argument("-d", "--dirty",
                    help="Keep temporary files (Graded files are always kept)", action="store_true")

args = parser.parse_args()

do_unpack = args.unpack or not (args.unpack or args.grade or args.pack)
do_grade = args.grade or not (args.unpack or args.grade or args.pack)
do_pack = args.pack or not (args.unpack or args.grade or args.pack)
do_clean = not args.dirty

if do_unpack and not do_grade and do_pack:
    print("\nUnpacking and repacking without grading doesn't make much sense, but whatever floats your boat.")


def unzip(zip_path, ungraded_path):
    if os.path.exists(ungraded_path):
        print("[SKIPPING]", ungraded_path, "already exists, skipping")
    else:
        subprocess.call(["unzip", zip_path, "-d", ungraded_path], stdout=subprocess.DEVNULL)


def unpack(ungraded_path, grading_path):
    groups_to_files = dict()

    for folder_name in os.listdir(ungraded_path):

        # Get the group name from the folder name
        group_match = GROUPNAME_REGEX.fullmatch(folder_name)
        if group_match is None:
            raise RuntimeError("'{}' does not match the Regex '{}'!".format(folder_name, GROUPNAME_REGEX))
        group_name = group_match.group("groupname")

        # Check if the group name is excluded
        if EXCLUDE_GROUPNAMES_REGEX.match(group_name) is not None:
            continue

        # We take one student from each group and read their filenames into groups_to_files
        if group_name not in groups_to_files:
            groups_to_files[group_name] = os.listdir(os.path.join(ungraded_path, folder_name))
            for submitted_file in os.listdir(os.path.join(ungraded_path, folder_name)):
                if os.path.exists(os.path.join(grading_path, group_name + "-" + submitted_file)):
                    # Don't overwrite anything in the grading folder
                    print("[SKIPPING]", os.path.join(grading_path, group_name + "-" + submitted_file),
                          "already exists, skipping")
                else:
                    shutil.copy(os.path.join(ungraded_path, folder_name, submitted_file),
                                os.path.join(grading_path, group_name + "-" + submitted_file))
                    if submitted_file.endswith(".zip"):
                        current_unzipped = os.path.join(grading_path, group_name + "-" + submitted_file)[
                                           :-4]
                        if os.path.exists(current_unzipped):
                            print("[SKIPPING]", current_unzipped, "already exists, skipping extraction.")
                        else:
                            # Also unzip submitted files
                            unzip(os.path.join(grading_path, group_name + "-" + submitted_file),
                                  current_unzipped)
                            for macos_folder in [f for f in os.listdir(current_unzipped) if f == "__MACOSX"]:
                                print("Removing annoying MACOSX folder ヽ(ಠ_ಠ)ノ")
                                shutil.rmtree(os.path.join(current_unzipped, macos_folder))
                            if len(os.listdir(current_unzipped)) == 1 and os.path.isdir(
                                    os.path.join(current_unzipped, os.listdir(current_unzipped)[0])):
                                one_folder = os.listdir(current_unzipped)[0]
                                print("Folder with only one folder in it. Unpacking.")
                                for f in os.listdir(os.path.join(current_unzipped, one_folder)):
                                    shutil.copy(os.path.join(current_unzipped, one_folder, f),
                                                os.path.join(current_unzipped, f))
                                shutil.rmtree(os.path.join(current_unzipped, one_folder))

    return groups_to_files


def grade(grading_path, own_csv_location, exercise):
    submissions = os.listdir(grading_path)
    submissions.sort()

    already_graded_once = False
    always_skip_grading = False

    for submission_name in submissions:
        if os.path.isdir(os.path.join(grading_path, submission_name)):
            # If there is a folder here, it belongs to a zip file that we unpacked before. Skip that and wait for the zip file.
            continue
        group_match = GROUPNAME_REGEX.fullmatch(submission_name)
        if group_match is None:
            raise RuntimeError("'{}' does not match the Regex '{}'!".format(submission_name, GROUPNAME_REGEX))
        group_name = group_match.group("groupname")

        # Ignore excluded
        if EXCLUDE_GROUPNAMES_REGEX.match(group_name) is not None:
            continue

        if input("Grading " + submission_name + "\n(Enter to grade, or type something and then Enter to skip)") != "":
            if already_graded_once:
                print(
                    "WARNING! You already graded another group. This will result in no entry in your own csv and, when transferring grades to a 0 for this group")
            continue

        if submission_name.endswith(".pdf"):
            # Just open PDF files
            subprocess.call([PDF_EDITOR, os.path.join(grading_path, submission_name)],
                            stdout=subprocess.DEVNULL)
        elif submission_name.endswith(".zip"):
            # If it's a zip file, it belongs to a folder that we unpacked before. We'll just handle the folder
            submission_folder_name = submission_name[:-4]  # Without .zip
            if os.path.isdir(os.path.join(grading_path, submission_folder_name)):
                extr_files = []
                for root, dirs, files in os.walk(os.path.join(grading_path, submission_folder_name)):
                    for f in files:
                        extr_files.append(os.path.join(root, f))
                #extr_files = os.listdir(os.path.join(grading_path, submission_folder_name))

                # ASM files are printed and executed
                asm_files = [f for f in extr_files if f.endswith(".asm")]
                if len(asm_files) > 1:
                    input("WARNING! " + str(len(asm_files)) + " ASM files!")
                for f in asm_files:
                    if input("ASM-File: " + f) == "":
                        subprocess.call(["cat", os.path.join(grading_path, submission_folder_name, f)])
                        try:
                            subprocess.call(["java", "-jar", "Mars4_5.jar", "sm", f],
                                            timeout=5)
                        except subprocess.TimeoutExpired:
                            print("Timeout reached!")

                # RKT files are opend in Dr.Racket
                rkt_files = [f for f in extr_files if f.endswith(".rkt")]
                if len(rkt_files) > 1:
                    input("WARNING! " + str(len(rkt_files)) + " RKT files!")
                for f in rkt_files:
                    if input("RKT-File: " + f) == "":
                        try:
                            subprocess.call([DR_RACKET, f])
                        except subprocess.TimeoutExpired:
                            print("Timeout reached!")

                # PDF files are just opened with a pdf viewer
                pdf_files = [f for f in extr_files if f.endswith(".pdf")]
                if len(asm_files) > 1:
                    input("WARNING! " + str(len(pdf_files)) + " PDF files!")
                for f in pdf_files:
                    if input("PDF-File: " + f) == "":
                        subprocess.call([PDF_EDITOR, f], stdout=subprocess.DEVNULL)

                handled_files = asm_files + rkt_files + pdf_files
                if len(extr_files) != len(handled_files):
                    print("There are more files in that folder!\n")
                    input("\n".join([other_file for other_file in extr_files if
                                     other_file not in handled_files]))
            else:
                input("ZIP not extracted. Grade manually. Waiting...")
        else:
            input("Not a pdf or zip. Grade manually. Waiting...")

        already_existing_rows = list()
        if os.path.exists(own_csv_location):
            with open(own_csv_location, "r") as dat:
                reader = csv.reader(dat)
                for row in reader:
                    already_existing_rows.append(row)

        skip_grading = always_skip_grading

        if not skip_grading:
            for i in range(len(already_existing_rows) - 1, -1, -1):
                r = already_existing_rows[i]
                if len(r) == 0:
                    del already_existing_rows[i]
                elif group_name == r[0]:
                    print(group_name, "is already graded", r[1], "with sinnvoll bearbeitet", r[2], "and with note",
                          "\"" + r[3] + "\"")
                    if input("Replace? (y/[n]) ") != "y":
                        print("Okay, skipping")
                        skip_grading = True
                    else:
                        print("Okay, regrading")
                        del already_existing_rows[i]

        if not skip_grading:
            grade = input("\n\nGrade for group '" + group_name + "', Exercise " + exercise + ": \n (x,xx) > ")

            if grade == "":
                if not already_graded_once:
                    print("Okay, skipping all grading")
                    always_skip_grading = True
                else:
                    print(
                        "WARNING! You already graded another group. This will result in no entry in your own csv and, when transferring grades to a 0 for this group")
            else:
                already_graded_once = True
                sinnvoll_bearbeitet = "y" if input(
                    "\nSinnvoll bearbeitet? (nix für ja, etwas für nein)> ") == "" else "n"

                if "," not in grade:
                    grade += ",00"
                already_existing_rows.append([group_name, grade, sinnvoll_bearbeitet, input("\nNotes\n> ")])

                with open(own_csv_location, "w") as dat:
                    writer = csv.writer(dat)
                    for row in already_existing_rows:
                        writer.writerow(row)


def repack(ungraded_path, groups_to_files, documents_resorted_path, documents_extracted_path, zip_name,
           final_zip_path):
    for submission_folder in os.listdir(ungraded_path):
        group_match = GROUPNAME_REGEX.fullmatch(submission_folder)
        if group_match is None:
            raise RuntimeError("'{}' does not match the Regex '{}'!".format(submission_folder, GROUPNAME_REGEX))
        current_group = group_match.group("groupname")

        # Ignore excluded
        if EXCLUDE_GROUPNAMES_REGEX.match(current_group) is not None:
            continue

        files_to_copy = groups_to_files[current_group]
        if os.path.exists(os.path.join(documents_resorted_path, submission_folder)):
            print(os.path.join(documents_resorted_path, submission_folder), "already exists, deleting")
            shutil.rmtree(os.path.join(documents_resorted_path, submission_folder))

        os.mkdir(os.path.join(documents_resorted_path, submission_folder))

        for ftc in files_to_copy:
            if ftc.endswith(".zip"):
                print("File is a zip file")
                if os.path.isdir(os.path.join(documents_extracted_path, current_group + "-" + ftc[:-4])):
                    files_in_folder = os.listdir(os.path.join(documents_extracted_path, current_group + "-" + ftc[:-4]))
                    pdf_files = [f for f in files_in_folder if f.endswith(".pdf")]
                    asm_files = [f for f in files_in_folder if f.endswith(".asm")]
                    if len(pdf_files) == 1:
                        print(ftc, "Only one pdf file. Including for feedback.")
                        shutil.copy(
                            os.path.join(documents_extracted_path, current_group + "-" + ftc[:-4], pdf_files[0]),
                            os.path.join(documents_resorted_path, submission_folder, pdf_files[0]))
                    elif len(pdf_files) > 1:
                        print("WARNING! Multiple pdf files in", ftc + ", I have no idea which one to include. Skipping")
                    elif len(asm_files) == 1:
                        print(ftc, "Only one pdf file. Including for feedback.")
                        shutil.copy(
                            os.path.join(documents_extracted_path, current_group + "-" + ftc[:-4], asm_files[0]),
                            os.path.join(documents_resorted_path, submission_folder, asm_files[0]))
                    else:
                        print("There are too many files for", ftc + ", but I can only include one. No idea what to do ¯\\_(ツ)_/¯")
                else:
                    print("WARNING! No folder for zip file", ftc + ". Excluding from feedback.")
            else:
                shutil.copy(os.path.join(documents_extracted_path, current_group + "-" + ftc),
                            os.path.join(documents_resorted_path, submission_folder, ftc))

    if os.path.exists(os.path.join(final_zip_path, zip_name)):
        print("Zip already exists, deleting...")
        os.remove(os.path.join(final_zip_path, zip_name))
    if LINUX:
        subprocess.call(["zip", "-r", os.path.join(final_zip_path, zip_name)] + os.listdir(documents_resorted_path),
                        cwd=documents_resorted_path, stdout=subprocess.DEVNULL)
    else:
        subprocess.call(["zip", "-r", os.path.abspath(os.path.join(final_zip_path, zip_name)), "*"],
                        cwd=documents_resorted_path, stdout=subprocess.DEVNULL)


def transfer_grades(exercise_path, official_csv_path, official_csv_name, own_csv_location):
    if os.path.exists(official_csv_path):
        print("Official grading file", official_csv_path, "found.")
        group_grades = dict()
        if os.path.exists(own_csv_location):
            print("Own grading file found. Transferring grades from notes file.")
            with open(own_csv_location, "r") as dat:
                reader = csv.reader(dat)
                for row in reader:
                    if len(row) > 0:
                        if row[0] in group_grades:
                            raise RuntimeError("This shouldn't happen. WTF?")
                        group_grades[row[0]] = row[1]

            with open(official_csv_path, "r") as from_csv, open(os.path.join(exercise_path, official_csv_name),
                                                                "w") as to_csv:
                reader = csv.reader(from_csv)
                writer = csv.writer(to_csv)

                first = True
                for line in reader:
                    if first:
                        # First line, find indices of relevant columsn
                        first = False
                        group_index = line.index(GROUP_COLUMN_NAME)
                        grade_index = line.index(GRADE_COLUMN_NAME)
                    else:
                        if EXCLUDE_GROUPNAMES_REGEX.match(line[group_index]) is not None:
                            continue # Ignore excluded
                        if line[group_index] not in group_grades:
                            print("\n>>>>> WARNING! Group '" + line[group_index] + "' has no grade!")
                            print("Assuming no submission and grading 0")
                            group_grades[line[group_index]] = "0,00"
                        elif line[grade_index].strip() != "" and line[grade_index].strip() != group_grades[line[group_index]]:
                            print("\n\n>>>>> VERY WARNING!", line[0],
                                  "was already graded differently before! Overriding.\n\n")
                        line[grade_index] = group_grades[line[group_index]]
                    writer.writerow(line)
        else:
            print("Own notes not found. Skipping grade transfer.")
    else:
        print("Official grading file", official_csv_path, "not found. Skipping grade transfer.")


def main():
    starting_time = datetime.datetime.now()
    working_dir = args.work_dir
    if FILEBROWSER is not None:
        filebrowser = subprocess.Popen([FILEBROWSER, working_dir], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

    exercises = [(p, os.path.join(working_dir, p)) for p in os.listdir(working_dir) if
                 re.fullmatch(ZIP_PREFIX + ".*\\.zip", p)]
    exercises.sort()

    try:
        for (zip_name, zip_path) in exercises:
            split_name = zip_name[len(ZIP_PREFIX):-4].split("-")
            exercise = "-".join(split_name[:-1])

            print("\n\n== Starting exercise " + exercise + " ==")
            if input("To continue, type enter. To skip, type anything. ") != "":
                print("Skipping")
                continue

            exercise_path = os.path.join(working_dir, exercise)
            ungraded_path = os.path.join(working_dir, exercise, UNGRADED_FOLDER)
            documents_extracted_path = os.path.join(working_dir, exercise, GRADING_FOLDER)
            documents_resorted_path = os.path.join(working_dir, exercise, RESORTED_FOLDER)
            own_csv_location = os.path.join(working_dir, exercise, NOTES_FILE)
            final_zip_path = os.path.join(working_dir, exercise)
            official_csv_name = "Bewertungen-" + zip_name.replace(".zip", ".csv")
            official_csv_path = os.path.join(working_dir, official_csv_name)

            if os.path.exists(exercise_path):
                print(exercise_path, "already exists, skipping")
            else:
                os.mkdir(exercise_path)

            if os.path.exists(documents_extracted_path):
                print(documents_extracted_path, "already exists, skipping")
            else:
                os.mkdir(documents_extracted_path)

            if os.path.exists(documents_resorted_path):
                print(documents_resorted_path, "already exists, skipping")
            else:
                os.mkdir(documents_resorted_path)

            groups_to_files = None

            if do_unpack or do_pack:
                if not do_unpack:
                    print("I know you didn't tell me to unpack, but I need to do it to repack. I don't care what you want.")
                else:
                    print("\nUnpacking")
                unzip(zip_path, ungraded_path)

                groups_to_files = unpack(ungraded_path, documents_extracted_path)

            if do_grade:
                print("\nGrading")
                grade(documents_extracted_path, own_csv_location, exercise)

            if do_pack:
                print("\nPacking")
                if not os.path.exists(ungraded_path):
                    print("I need the extracted zip to copy the paths")
                    unzip(zip_path, ungraded_path)

                repack(ungraded_path, groups_to_files, documents_resorted_path, documents_extracted_path, zip_name,
                       final_zip_path)

                transfer_grades(exercise_path, official_csv_path, official_csv_name, own_csv_location)

            if do_clean:
                print("\nCleaning up")
                shutil.rmtree(ungraded_path)
                shutil.rmtree(documents_resorted_path)
    except KeyboardInterrupt:
        print("\n\nOkay, bye\n")


    if FILEBROWSER is not None:
        filebrowser.terminate()
    end_time = datetime.datetime.now()
    elapsed_time = end_time - starting_time
    print("You worked from", starting_time.strftime("%H:%M:%S"), "to", end_time.strftime("%H:%M:%S"))
    if end_time.hour < 4:
        print("It's late, you should get some sleep now. Your mother and I are worried about you.")
    print("Duration:", datetime.timedelta(seconds=math.ceil(elapsed_time.total_seconds())))


if __name__ == "__main__":
    main()
