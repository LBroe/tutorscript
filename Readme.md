# Tutorscript

A script that helps tutors grade exercises submitted on [LearnWeb](https://www.uni-muenster.de/LearnWeb/learnweb2/) (may work on other Moodle platforms too, I don't know). It was supposed to be small but got pretty messy pretty fast. Pull requests are welcome.

If you want to grade an excercise on LearnWeb, you can download the submissions and the grading worksheet for the excercise. You can then insert feedback into the files, fill the grading worksheet and upload both to the site.
This process is annoying for a few different reasons:

 - You have to unzip and rezip the submissions every time. They are often zip archives themselves, so you have to unzip them too. The participants also inconsistently name their files or put them in folders.
 - Often, participants are divided into groups that give one submission together. LearnWeb doesn't care about that and gives you a copy of their submission for every group member. So you have to pick one, grade it and distribute it to the other members so they can see your feedback.
 - The grading worksheet is big and you don't want to insert everything manually.

This script aims to take care of all of that for you. You put all of the exercises and the grading sheet into one folder and the script puts everything into folders divided by exercise and subdivided by group where you can grade it all. It interactively walks you through all of the groups and you can insert a grade.
After that, it repackages the annotated files and fills out the grading worksheets and both into the exercise folders. Because the results are kept in an notes file, you can just Ctrl+C out any time and resume later.

It also tries to open the PDF files or test some code that it finds, but this process may need some individual attention because of different pdf editors or operating systems. Just look at the `grade` function and Frankenstein in what you need, that's what I'm doing.


To start, just type `python3 tutorscript.py <location of the files>`.